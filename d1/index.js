// // Synchronous and Asynchronous

// 	// Javascript is by default synchronous meaning it only runs one statement at a time.

// 	// Javascript is single-threaded.

// 	// TASK A -> TASK B -> TASK C

// // console.log('Hello World')

// // console.log('Hello Again')

// // for (let i = 0; i <= 1500; i++){
// // 	console.log(i)
// // }

// // console.log('The End')



// // When an action will take some time to process, this results in code "blocking"

// // Asynchronous

// 	// means that we can proceed to execute other statements, while time consuming codes is running in the background.

// // Getting all posts

// 	/*
// 		Syntax:
// 			fetch('URL')
// 			console.log(fetch())
// 	*/

// 	// fetch('https://jsonplaceholder.typicode.com/posts')
// 	// 	// fetch allows you to asynchronously request for data

// 	// 	console.log(fetch('https://jsonplaceholder.typicode.com/'))

// 	// /*
// 	// 	Syntax:
// 	// 		fetch('URL')
// 	// 		.then((response) => {})
// 	// 		console.log(fetch())
// 	// */

// 	// fetch('https://jsonplaceholder.typicode.com/posts')
// 	// 	.then((response) => console.log(response.status))

// 	// 	// captures the response object and return another promise which will eventually be resolved or rejected.

// 	// fetch('https://jsonplaceholder.typicode.com/posts')
// 	// 	.then((response) => response.json())

// 	// 	// json method from response object to convert the data retrieved into JSON format used in our application.
// 	// 	.then((json) => console.log(json))

// 		// print the converted JSON value from the fetch request.

// 	// Using multiple ".then" methods creates a "promis chain"

// 	// Async - Await

	async function fetchData(){
		let result = await fetch('https://jsonplaceholder.typicode.com/posts')

		console.log(result);

		console.log(typeof(result));

		console.log(result.body);

		let json = await result.json()

		console.log(json)

	};

// 	// fetchData();

// // Syntactic Sugar
// 	// makes the code more readable
// 	// but not necessarily better

// // Async-Await is the syntactic sugar of .then method

// 	// Retrieve specific post
// 	fetch('https://jsonplaceholder.typicode.com/posts/14')
// 	.then((response) => response.json())
// 	.then((json) => console.log(json))


// // Getting a specific post

// 	// Syntax:
// 		// fetch ('URL', options)
// 		// .then((response) => {})
// 		// .then((response) => {})


// // Creating a specific post

// 	// Syntax:
// 		// fetch ('URL', options)
// 		// .then((response) => {})
// 		// .then((response) => {})

// 	fetch('https://jsonplaceholder.typicode.com/posts', {
// 		method: 'POST',
// 		headers: {
// 			'Content-Type': 'application.json'
// 		},
// 		body: JSON.stringify({
// 			title: 'New Post',
// 			body: 'Hello World',
// 			userId: 1
// 		})
// 	})
// 	.then((response) => response.json())
// 	.then((json) => console.log(json))


// // Update a post

	// fetch('https://jsonplaceholder.typicode.com/posts/14', {
	// 	method: 'PUT',
	// 	headers: {
	// 		'Content-Type': 'application/json'
	// 	},
	// 	body: JSON.stringify ({
	// 		id: 1,
	// 		title: 'Updated Post',
	// 		body: 'Hello Once More',
	// 		userID: 1
	// 	})

	// })
	// .then((response) => response.json())
	// .then((json) => console.log(json))


// // updating post with PATCH

// 	fetch('https://jsonplaceholder.typicode.com/posts/14', {
// 		method: 'PATCH',
// 		headers: {
// 			'Content-Type': 'application/json'
// 		},
// 		body: JSON.stringify ({
// 			title: 'Corrected Post'
// 		})

// 	})
// 	.then((response) => response.json())
// 	.then((json) => console.log(json))

// // PUT and PATCH

// // deleting a post



// 	// fetch('https://jsonplaceholder.typicode.com/posts/1',{
// 	// 	method: 'DELETE'
// 	// })

// 	fetch('https://jsonplaceholder.typicode.com/posts/1',{
// 		method: 'DELETE'
// 	})
	
// 	.then((response) => response.json())
// 	.then((json) => console.log(json))
