//3. - OK

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'GET',
})
.then((response) => response.json())
.then((json) => console.log(json))



// 4.



//5. -OK
fetch('https://jsonplaceholder.typicode.com/todos/101', {
	method: 'GET',
})
.then((response) => response.json())
.then((json) => console.log(json))


//6.- OK 

fetch('https://jsonplaceholder.typicode.com/todos/101')
    .then((response) => response.json())
    .then((json) => console.log('title:' + json['title'] + ', '  + 'completed:' + json['completed']))



// 7. - OK

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 100,
		id: 500,
		title: 'New Post'
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


//8. - OK

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 2,
		title: 'Updated title',
	})
})
.then((response) => response.json())
.then((json) => console.log(json))


//9.

fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify ({
		description: 'additional description',
		dateCompleted: 'October 25, 2021'
	})

})
.then((response) => response.json())
.then((json) => console.log(json))



 //10. - OK

fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'PATCH',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify ({
		title: 'Updated title'
	})

})
.then((response) => response.json())
.then((json) => console.log(json))

//11. - OK

fetch('https://jsonplaceholder.typicode.com/todos/4', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		completed: true,
		date: 'October 25, 2021'
	})
})
.then((response) => response.json())
.then((json) => console.log(json))



